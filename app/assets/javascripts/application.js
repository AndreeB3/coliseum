// This is a manifest file that'll be compiled into application.js, which will include all the files
// listed below.
//
// Any JavaScript/Coffee file within this directory, lib/assets/javascripts, or any plugin's
// vendor/assets/javascripts directory can be referenced here using a relative path.
//
// It's not advisable to add code directly here, but if you do, it'll appear at the bottom of the
// compiled file. JavaScript code in this file should be added after the last require_* statement.
//
// Read Sprockets README (https://github.com/rails/sprockets#sprockets-directives) for details
// about supported directives.
//
//= require activestorage
//= require turbolinks
//= require jquery
//= require jquery3
//= require jquery_ujs
//= require jquery-ui/widgets/sortable
//= require rails_sortable
//= require nested_form_fields
//= require_tree .


console.log("foo")

$(document).ready(function () {
    
    $("#filter-season").change(function () {
        var action = "/seasons/" + $(this).val();
        console.log(action);
        $("#filter-form").attr("action",action);
        $("#filter-form").submit();
    });

    $(".keyTest").click(function () {
        var index = $(".friendsSize").last().val();
        var newIndex = index +1 ;

        $(".form_friend").clone().appendTo("#form-friend");

    });

    $(".tab_content").hide(); //Hide all content
    $("ul.tabs li:first").addClass("active").show(); //Activate first tab
    $(".tab_content:first").show(); //Show first tab content

    //On Click Event
    $("ul.tabs li").click(function () {

        $("ul.tabs li").removeClass("active"); //Remove any "active" class
        $(this).addClass("active"); //Add "active" class to selected tab
        $(".tab_content").hide(); //Hide all tab content

        var activeTab = $(this).find("a").attr("href"); //Find the href attribute value to
        $(activeTab).fadeIn(); //Fade in the active ID content
        return false;
    });

    $("#filter-roosters").change(function () {
        console.log("fsadas");
        $(".rooster").hide();

        seassonid = $(this).val();
        $("."+seassonid+"").show();
    });

    $("#btnFullSC").click(function (){
        $(".replay").removeClass("container");
        $(".replay").addClass("fullScream");

    });
     
    $(".btn-menu-tournaments").click(function () {
        $(".menu-tournaments").addClass("hidden");
        var menuName = $(this).data("name");
        $("#"+menuName+"").removeClass("hidden");

    });

    $(".btn-menu-seasons").click(function () {
        $(".menu-seasons").addClass("hidden");
        var menuName = $(this).data("name");
        $("#" + menuName + "").removeClass("hidden");

    });

    $(".libras, .onzas").change(function () {
        var index = $(this).data("index");
        var weight = $("#libras"+index+"").val() + "." + $("#onzas"+index+"").val();
        $("#weight"+index+"").val(weight);
        console.log("foo")
    });
});
