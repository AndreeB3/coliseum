class SeasonsController < ApplicationController
  
  before_action :get_season, only: [:show, :edit, :update, :destroy]

  def index
    @seasons = Season.all
  end

  def show
    @rooster = Rooster.new
    @barns = Array.new
    _barns = Barn.where( tournament_id: @season.tournament_id)
    _roosters = Array.new
    
    _barns.each do |barn|
      barn.roosters.each  do |rooster|
        if rooster.season_id == @season.id 
          @barns.push(barn)
          break
        end
      end
    end
    @notSelected = Array.new
    @season.roosters.each do |rooster|
      if rooster.fight_id == nil
        @notSelected.push(rooster)
      end
    end
    
  
  end

  def new
    @season = Season.new
    @season.tournament_id = params[:format]
    @tournaments = Tournament.all
  end

  def edit
  end

  def create
    
    @season = Season.new(season_params)
  

    respond_to do |format|
      if @season.save
        format.html { redirect_to tournament_path(@season.tournament), notice: "Fecha creada" }
      else
        format.html { render :new }
      end
    end
  end

  def update
    respond_to do |format|
      if @season.update(season_params)
        format.html { redirect_to season_path(@season), notice: "Categoría actualizada exitósamente" }
      else
        format.html { render :edit }
      end
    end
  end

  def destroy
    @season.destroy

    respond_to do |format|
      format.html { redirect_to tournament_path(@season.tournament_id) }
    end
  end

  private
    def get_season
      @season = Season.find(params[:id])
    end

    def season_params
      params.require(:season).permit(:name, :tournament_id)
    end

end
