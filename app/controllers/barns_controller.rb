class BarnsController < ApplicationController
  
  before_action :get_seasons, only: [:new, :index, :update, :destroy, :edit, :show]
  before_action :get_barn, only: [:destroy, :edit, :update, :show]

  def index
    @barns = Barn.all
  end

  def show
  end

  def new
    @barn = Barn.new
    @barn.tournament_id = params[:format]

    @seasons = Season.where(tournament_id: @barn.tournament_id).order('created_at DESC') 

    if @seasons.empty?
      redirect_to tournament_path(@barn.tournament_id), notice: "Primero debe ingreser una fecha." 
    end
    @times = @barn.tournament.quantity 
    @times.times {@barn.roosters.build} 
    3.times{@barn.friends.build}
    
  end

  def edit
    @season = Season.find(@barn.season_id)
  end

  def create

    @barn = Barn.create(barn_params)

    respond_to do |format|
      if @barn.save
        format.html { redirect_to tournament_path(@barn.tournament_id), notice: "Galpon creado exitósamente" }
      else
        puts @barn.errors.full_messages
        format.html { render :new }
      end
    end
  end

  def update
    respond_to do |format|
      if @barn.update(barn_params)
        format.html { redirect_to tournament_path(@barn.tournament_id), notice: "Galponn actualizada exitósamente" }
      else
        format.html { render :edit }
      end
    end
  end

  def destroy
    
    @barn.destroy

    respond_to do |format|
      format.html { redirect_to season_path(@season_id) }
    end
  end

  private
    def get_seasons 
      @seasons = Season.all
    end

    def get_barn
      @barn = Barn.find(params[:id])
    end

    def barn_params
      params.require(:barn).permit(:name, :tournament_id, :place , roosters_attributes: [:id, :name , :weight, :plate, :race, :fight_id, :season_id], friends_attributes: [:id, :friend])
    end

end
