class FightsController < ApplicationController

  before_action :get_fight, only: [:edit, :update, :show]
    
  def index
    @season = Season.find(params[:format])
    @fights = @season.fights
    puts "##############################"
  end

  def new 

    @season = Season.find(params[:format])
    @season.fights.each do |fight|
      fight.roosters.each do |rooster|
        rooster.update({
              :name => rooster.name,
              :weight => rooster.weight,
              :plate => rooster.plate,
              :race => rooster.race,
              :barn_id => rooster.barn_id,
              :fight_id => nil,
              :played => rooster.played,
              :score => rooster.score
            })
      end
      fight.destroy
    end

    @allRossters = Array.new 
    @rooster30 = Array.new
    @rooster31 = Array.new
    @rooster32 = Array.new
    @rooster33 = Array.new
    @rooster34 = Array.new
    @rooster35 = Array.new
    @rooster36 = Array.new
    @rooster37 = Array.new
    @rooster38 = Array.new
    @rooster39 = Array.new
    @rooster310 = Array.new
    @rooster311 = Array.new
    @rooster312 = Array.new
    @rooster313 = Array.new
    @rooster314 = Array.new
    @rooster315 = Array.new
    @rooster40 = Array.new
    @rooster41 = Array.new
    @rooster42 = Array.new
    @rooster43= Array.new
    @rooster44 = Array.new
    @rooster45 = Array.new
    @rooster46 = Array.new
    @rooster47 = Array.new
    @rooster48 = Array.new
    @rooster49 = Array.new
    @rooster410 = Array.new
    @rooster411 = Array.new
    @rooster412 = Array.new
    @rooster413 = Array.new
    @rooster414 = Array.new
    @rooster415 = Array.new
    @rooster50 = Array.new
    @remaining = Array.new
    @notSelected = Array.new
    @fightNumber = 0
    
    
    @season.roosters.each do |rooster|
        if rooster.weight == '3.0'
          @rooster30.push(rooster)
        elsif rooster.weight == '3.1'
          @rooster31.push(rooster)
        elsif rooster.weight == '3.2'
          @rooster32.push(rooster)
        elsif rooster.weight == '3.3'
          @rooster33.push(rooster)
        elsif rooster.weight == '3.4'
          @rooster34.push(rooster)
        elsif rooster.weight == '3.5'
          @rooster35.push(rooster)
        elsif rooster.weight == '3.6'
          @rooster36.push(rooster)
        elsif rooster.weight == '3.7'
          @rooster37.push(rooster)
        elsif rooster.weight == '3.8'
          @rooster38.push(rooster)
        elsif rooster.weight == '3.9'
          @rooster38.push(rooster)
        elsif rooster.weight == '3.10'
          @rooster39.push(rooster)
        elsif rooster.weight == '3.11'
          @rooster311.push(rooster)
        elsif rooster.weight == '3.12'
          @rooster312.push(rooster)
        elsif rooster.weight == '3.13'
          @rooster313.push(rooster)
        elsif rooster.weight == '3.14'
          @rooster314.push(rooster)
        elsif rooster.weight == '3.15'
          @rooster315.push(rooster)
        elsif rooster.weight == '4.0'
          @rooster40.push(rooster)
        elsif rooster.weight == '4.1'
          @rooster41.push(rooster)
        elsif rooster.weight == '4.2'
          @rooster42.push(rooster)
        elsif rooster.weight == '4.3'
          @rooster43.push(rooster)
        elsif rooster.weight == '4.4'
          @rooster44.push(rooster)
        elsif rooster.weight == '4.5'
          @rooster45.push(rooster)
        elsif rooster.weight == '4.6'
          @rooster46.push(rooster)
        elsif rooster.weight == '4.7'
          @rooster47.push(rooster)
        elsif rooster.weight == '4.8'
          @rooster48.push(rooster)
        elsif rooster.weight == '4.9'
          @rooster49.push(rooster)
        elsif rooster.weight == '4.10'
          @rooster410.push(rooster)
        elsif rooster.weight == '4.11'
          @rooster411.push(rooster)
        elsif rooster.weight == '4.12'
          @rooster412.push(rooster)
        elsif rooster.weight == '4.13'
          @rooster413.push(rooster)
        elsif rooster.weight == '4.14'
          @rooster414.push(rooster)
        elsif rooster.weight == '4.15'
          @rooster415.push(rooster)
        else 
          @rooster50.push(rooster)
        end
    end

    ##Sortea los de 3.0 y 3.1 y sorteo el resto de los dos 
    sortFight(@rooster30)
    sortFight(@rooster31)
    sortRemaining(@remaining)

    sortFight(@rooster32)
    sortRemaining(@remaining)

    sortFight(@rooster33)
    @remaining = excludeRoosters(@remaining, '3.0')
    sortRemaining(@remaining)
    logger.info("#########################3.4")
    logger.info(@rooster34.to_json)
    sortFight(@rooster34)
    @remaining = excludeRoosters(@remaining, '3.1')
    sortRemaining(@remaining)

    sortFight(@rooster35)
    @remaining = excludeRoosters(@remaining, '3.2')
    sortRemaining(@remaining)

    sortFight(@rooster36)
    @remaining = excludeRoosters(@remaining, '3.3')
    sortRemaining(@remaining)

    sortFight(@rooster37)
    @remaining = excludeRoosters(@remaining, '3.4')
    sortRemaining(@remaining)

    sortFight(@rooster38)
    @remaining = excludeRoosters(@remaining, '3.5')
    sortRemaining(@remaining)

    sortFight(@rooster39)
    @remaining = excludeRoosters(@remaining, '3.6')
    sortRemaining(@remaining)

    sortFight(@rooster310)
    @remaining = excludeRoosters(@remaining, '3.7')
    sortRemaining(@remaining)

    sortFight(@rooster311)
    @remaining = excludeRoosters(@remaining, '3.8')
    sortRemaining(@remaining)

    sortFight(@rooster312)
    @remaining = excludeRoosters(@remaining, '3.9')
    sortRemaining(@remaining)

    sortFight(@rooster313)
    @remaining = excludeRoosters(@remaining, '3.10')
    sortRemaining(@remaining)

    sortFight(@rooster314)
    @remaining = excludeRoosters(@remaining, '3.11')
    sortRemaining(@remaining)

    sortFight(@rooster315)
    @remaining = excludeRoosters(@remaining, '3.12')
    sortRemaining(@remaining)

    sortFight(@rooster40)
    @remaining = excludeRoosters(@remaining, '3.13')
    sortRemaining(@remaining)

    sortFight(@rooster41)
    @remaining = excludeRoosters(@remaining, '3.14')
    sortRemaining(@remaining)

    sortFight(@rooster42)
    @remaining = excludeRoosters(@remaining, '3.15')
    sortRemaining(@remaining)

    sortFight(@rooster43)
    @remaining = excludeRoosters(@remaining, '4.0')
    sortRemaining(@remaining)

    sortFight(@rooster44)
    @remaining = excludeRoosters(@remaining, '4.1')
    sortRemaining(@remaining)

    sortFight(@rooster45)
    @remaining = excludeRoosters(@remaining, '4.2')
    sortRemaining(@remaining)

    sortFight(@rooster46)
    @remaining = excludeRoosters(@remaining, '4.3')
    sortRemaining(@remaining)

    sortFight(@rooster47)
    @remaining = excludeRoosters(@remaining, '4.4')
    sortRemaining(@remaining)

    sortFight(@rooster48)
    @remaining = excludeRoosters(@remaining, '4.5')
    sortRemaining(@remaining)

    sortFight(@rooster49)
    @remaining = excludeRoosters(@remaining, '4.6')
    sortRemaining(@remaining)

    sortFight(@rooster410)
    @remaining = excludeRoosters(@remaining, '4.7')
    sortRemaining(@remaining)

    sortFight(@rooster411)
    @remaining = excludeRoosters(@remaining, '4.8')
    sortRemaining(@remaining)

    sortFight(@rooster412)
    @remaining = excludeRoosters(@remaining, '4.9')
    sortRemaining(@remaining)

    sortFight(@rooster413)
    @remaining = excludeRoosters(@remaining, '4.10')
    sortRemaining(@remaining)

    sortFight(@rooster414)
    @remaining = excludeRoosters(@remaining, '4.11')
    sortRemaining(@remaining)

    sortFight(@rooster415)
    @remaining = excludeRoosters(@remaining, '4.12')
    sortRemaining(@remaining)

    sortFight(@rooster50)
    @remaining = excludeRoosters(@remaining, '4.13')
    sortRemaining(@remaining)

    @notSelected += @remaining
    _fights = Fight.where(season_id: @season.id)
    _fights = _fights.to_a
    fightNumber = 0
    logger.info( _fights.count-1)
    for i in 0.._fights.count-1
      randomNum = Random.rand(_fights.count)
      _f = _fights.delete_at(randomNum)
      logger.info("##########PASO######")
      logger.info(_fights.count)
      logger.info(_fights)
      logger.info(_f.to_json)
      @f_for_update = Fight.where(id: _f.id)
      @f_for_update.update(
        :number => i+1
      )
    end

    @fights = Fight.where(season_id: @season.id).order(:number)

    redirect_to season_path(@season)

  end 
  def show
  end
  def edit 
  end

  def update

    respond_to do |format|
      if @fight.update(fight_params)

        @fight.roosters.each do |rooster|
          rooster.played = true
          if @fight.winner == 0 
            rooster.score = 1 
          elsif @fight.winner == rooster.id 
            if @fight.pollon?
              rooster.score =  4
            else 
              rooster.score = 3
            end
          else 
            rooster.score = 0
          end
          update_rooster(rooster)
        end

        format.html { redirect_to season_path(@fight.season), notice: "Categoría actualizada exitósamente" }
      else
        format.html { render :edit }
      end
    end
  end

  private 
  def update_rooster(rooster)
    @roosterUp = Rooster.find(rooster.id)
    @roosterUp.update({
              :name => rooster.name,
              :weight => rooster.weight,
              :plate => rooster.plate,
              :race => rooster.race,
              :barn_id => rooster.barn_id,
              :fight_id => rooster.fight_id,
              :played => rooster.played,
              :score => rooster.score
            })
  end
  def get_fight
    @fight = Fight.find(params[:id])
  end

  def fight_params
      params.require(:fight).permit(:number, :winner, :pollon, :time)
  end

  def available?(gallo1, gallo2)
    
    @friends1 = Friend.where(barn_id: gallo1.barn_id)
    @friends2 = Friend.where(barn_id: gallo2.barn_id)

    if gallo1.barn_id == gallo2.barn_id
      return false 
    elsif isFriend?(@friends1,gallo2.barn_id)
      return false 
    elsif isFriend?(@friends2,gallo1.barn_id)
      return false 
    else 
      true 
    end
  end

  def isFriend?(friends, id)
    friends.each do |friend|
      if friend.friend != nil and friend.friend == id
        return true
      end
    end
    false
  end

  def excludeRoosters(roosters, str)
    roosters.each_with_index do |rooster,index|
      if rooster.weight === str
        @notSelected.push(roosters.delete_at(index))
      end
    end
    return roosters
  end
  
  def sortRemaining(remainingRoosters)
    logger.info("#######REMAINING#########")
    logger.info(remainingRoosters.to_json)
    logger.info("#########################")
    if remainingRoosters.count > 1
      @remaining2 = Array.new
      @remaining2 += remainingRoosters
      @remaining.clear
      sortFight(@remaining2)
    end
    
  end

  def sortFight(roosters)
    while roosters.count > 1 do 
      _randomNum1 = Random.rand(roosters.count)
      @gallo1 = roosters.delete_at(_randomNum1)
      _randomNum2 = Random.rand(roosters.count)
      @gallo2 = roosters[_randomNum2]
      if available?(@gallo1, @gallo2)
        @gallo2 = roosters.delete_at(_randomNum2)
        createFlight(@gallo1, @gallo2)
      else
        _randomNums = Array.new
        _randomNums.push(_randomNum2)
        
        while _randomNums.count < roosters.count do
          _randomNum2 = Random.rand(roosters.count)
          while _randomNums.include?(_randomNum2)
            _randomNum2 = Random.rand(roosters.count)
          end
          _randomNums.push(_randomNum2)
          @gallo2 = roosters[_randomNum2]
          if available?(@gallo1, @gallo2)
            @gallo2 = roosters.delete_at(_randomNum2)
            createFlight(@gallo1, @gallo2)
            break
          end
        end
        @remaining.push(@gallo1)
      end
    end
    @remaining += roosters
  end

  def createFlight(rooster1,rooster2)

    @fightNumber += 1

    @fight = Fight.new({
            :number => @fightNumber,
            :season_id => @season.id,
            :winner => nil,
            :time => '',
            :pollon => nil
        })
    @fight.save
    @roosterUp = Rooster.find(rooster1.id)
    @roosterUp.update({
          :name => rooster1.name,
          :weight => rooster1.weight,
          :plate => rooster1.plate,
          :race => rooster1.race,
          :barn_id => rooster1.barn_id,
          :fight_id => @fight.id
        })
    @roosterUp2 = Rooster.find(rooster2.id)
    @roosterUp2.update({
            :name => rooster2.name,
            :weight => rooster2.weight,
            :plate => rooster2.plate,
            :race => rooster2.race,
            :barn_id => rooster2.barn_id,
            :fight_id => @fight.id
        })
  end
  
end