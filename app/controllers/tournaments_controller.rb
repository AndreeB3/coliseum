class TournamentsController < ApplicationController
  
  before_action :get_tournament, only: [:show, :edit, :update, :destroy]

  def index
    @tournaments = Tournament.all
  end

  def show

    @seasons = Season.where(tournament_id: @tournament.id).order('created_at DESC')
    @rooster = Rooster.new
    @barns = @tournament.barns.sort_by { |m| m.name.downcase }
    @barnsScore = @tournament.barns.sort_by { |m| m.score }

    @barns.each do |barn|
      if barn.score == nil
        barn.score = 0
        barn.totalTime = 0 
      end
      barn.roosters.each do |rooster|
        if rooster.played?
          if barn.played == nil
            barn.played = 0
            barn.lost = 0
            barn.tie = 0
            barn.won = 0
            barn.score = 0
            barn.pollon = 0
            barn.totalTime= 00
          end
          barn.played += 1 

          case rooster.score
          when 0
            barn.lost += 1 
          when 1
            barn.tie += 1
            barn.score += 1 
          when 3
            barn.won += 1
            barn.score += 3
            barn.totalTime += timeToSec(rooster.fight.time)
          when 4 
            barn.pollon += 1 
            barn.score += 4 
            barn.totalTime += timeToSec(rooster.fight.time)
          end
        end
      end
    end
    @barnsScore = @barns
    @barnsScore = @barnsScore.sort_by { |m| [ -m.score, m.totalTime.to_i ] }

  end

  def timeToSec(time)
    return ( time.split(":")[0].to_i * 60 ) + time.split(":")[1].to_i 
  end

  def calculateTotalTime(totalTime, fightTime)
    
    _totalTimeToSec = ( totalTime.split(":")[0].to_i * 60 ) + totalTime.split(":")[1].to_i 
    _fightTimeToSec = ( fightTime.split(":")[0].to_i * 60 ) + fightTime.split(":")[1].to_i 

    return _totalTimeToSec + _fightTimeToSec
  end
  def new
    @tournament = Tournament.new
  end

  def edit
  end

  def create
    @tournament = Tournament.new(tournament_params)

    respond_to do |format|
      if @tournament.save
        format.html { redirect_to tournaments_path, notice: "Categoría creada exitósamente" }
      else
        format.html { render :new }
      end
    end
  end

  def update
    respond_to do |format|
      if @tournament.update(season_params)
        format.html { redirect_to tournament_path(@tournament), notice: "Categoría actualizada exitósamente" }
      else
        format.html { render :edit }
      end
    end
  end

  def destroy
    @tournament.destroy

    respond_to do |format|
      format.html { redirect_to tournaments_path }
    end
  end

  private
    def get_tournament
      @tournament = Tournament.find(params[:id])
    end

    def tournament_params
      params.require(:tournament).permit(:name,:quantity)
    end

end
