class RoostersController < ApplicationController
  
  before_action :get_rooster, only: []

  def create
    @rooster = Rooster.new(rooster_params)

    _count = 0 
    @rooster.barn.roosters.each do |rooster| 
      if rooster.season_id == @rooster.season_id
        _count += 1 
      end
    end

    _quantity = @rooster.barn.tournament.quantity

    respond_to do |format|
      if _quantity > _count
        if @rooster.save
          format.html { redirect_to tournament_path(@rooster.barn.tournament), notice: "Gallo agregado" }
        else
          format.html { render :new }
        end
      else
        format.html { redirect_to tournament_path(@rooster.barn.tournament), notice: "No se registro el gallo porque ya tiene gallos registrados para esta fecha."}
      end
    end
  end


  private

    def rooster_params
      params.require(:rooster).permit(:barn_id, :season_id, :race, :plate, :weight)
    end

end
