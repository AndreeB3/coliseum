class ApplicationController < ActionController::Base

    before_action :set_seasons


    private 

    def set_seasons
        @tournamentsBar = Tournament.order("created_at").last
    end
end
