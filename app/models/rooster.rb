class Rooster < ApplicationRecord
  belongs_to :barn
  belongs_to :fight , optional: true 
  belongs_to :season
end
