class Fight < ApplicationRecord
  belongs_to :season
  has_many :roosters
end
