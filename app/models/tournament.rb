class Tournament < ApplicationRecord
    has_many :seasons
    has_many :barns
end
