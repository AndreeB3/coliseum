class Season < ApplicationRecord
    belongs_to :tournament
    
    has_many :fights, :dependent => :delete_all
    has_many :roosters, :dependent => :delete_all
end
