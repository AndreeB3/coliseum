class Barn < ApplicationRecord
  belongs_to :tournament
  
  has_many :roosters, :dependent => :delete_all
  has_many :friends, :dependent => :delete_all
  accepts_nested_attributes_for :roosters, :allow_destroy => true
  accepts_nested_attributes_for :friends, :allow_destroy => true
end
