# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 2019_03_13_153533) do

  create_table "barns", options: "ENGINE=InnoDB DEFAULT CHARSET=utf8", force: :cascade do |t|
    t.string "name"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "place"
    t.bigint "tournament_id"
    t.integer "score"
    t.integer "played"
    t.integer "won"
    t.integer "lost"
    t.integer "tie"
    t.integer "pollon"
    t.integer "totalTime"
    t.index ["tournament_id"], name: "index_barns_on_tournament_id"
  end

  create_table "fights", options: "ENGINE=InnoDB DEFAULT CHARSET=utf8", force: :cascade do |t|
    t.integer "number"
    t.string "time"
    t.integer "winner"
    t.boolean "pollon"
    t.bigint "season_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["season_id"], name: "index_fights_on_season_id"
  end

  create_table "friends", options: "ENGINE=InnoDB DEFAULT CHARSET=utf8", force: :cascade do |t|
    t.integer "friend"
    t.bigint "barn_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["barn_id"], name: "index_friends_on_barn_id"
  end

  create_table "roosters", options: "ENGINE=InnoDB DEFAULT CHARSET=utf8", force: :cascade do |t|
    t.string "name"
    t.string "weight"
    t.string "plate"
    t.string "race"
    t.bigint "barn_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.bigint "fight_id"
    t.bigint "season_id"
    t.boolean "played"
    t.integer "score"
    t.index ["barn_id"], name: "index_roosters_on_barn_id"
    t.index ["fight_id"], name: "index_roosters_on_fight_id"
    t.index ["season_id"], name: "index_roosters_on_season_id"
  end

  create_table "seasons", options: "ENGINE=InnoDB DEFAULT CHARSET=utf8", force: :cascade do |t|
    t.string "name"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.bigint "tournament_id"
    t.index ["tournament_id"], name: "index_seasons_on_tournament_id"
  end

  create_table "tournaments", options: "ENGINE=InnoDB DEFAULT CHARSET=utf8", force: :cascade do |t|
    t.string "name"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.integer "quantity"
  end

  create_table "users", options: "ENGINE=InnoDB DEFAULT CHARSET=utf8", force: :cascade do |t|
    t.string "email", default: "", null: false
    t.string "encrypted_password", default: "", null: false
    t.string "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["email"], name: "index_users_on_email", unique: true
    t.index ["reset_password_token"], name: "index_users_on_reset_password_token", unique: true
  end

  add_foreign_key "barns", "tournaments"
  add_foreign_key "fights", "seasons"
  add_foreign_key "friends", "barns"
  add_foreign_key "roosters", "barns"
  add_foreign_key "roosters", "fights"
  add_foreign_key "roosters", "seasons"
  add_foreign_key "seasons", "tournaments"
end
