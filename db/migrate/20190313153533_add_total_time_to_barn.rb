class AddTotalTimeToBarn < ActiveRecord::Migration[5.2]
  def change
    add_column :barns, :totalTime, :integer
  end
end
