class AddWonToBarn < ActiveRecord::Migration[5.2]
  def change
    add_column :barns, :won, :integer
  end
end
