class RemoveSeasonFromBarns < ActiveRecord::Migration[5.2]
  def change
    remove_reference :barns, :season, foreign_key: true
  end
end
