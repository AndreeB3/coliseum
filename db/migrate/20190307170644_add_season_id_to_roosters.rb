class AddSeasonIdToRoosters < ActiveRecord::Migration[5.2]
  def change
    add_reference :roosters, :season, foreign_key: true
  end
end
