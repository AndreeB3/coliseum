class AddTournamentIdToBarns < ActiveRecord::Migration[5.2]
  def change
    add_reference :barns, :tournament, foreign_key: true
  end
end
