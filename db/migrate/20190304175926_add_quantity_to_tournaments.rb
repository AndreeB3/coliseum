class AddQuantityToTournaments < ActiveRecord::Migration[5.2]
  def change
    add_column :tournaments, :quantity, :integer
  end
end
