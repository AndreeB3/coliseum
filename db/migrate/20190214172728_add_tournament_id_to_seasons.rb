class AddTournamentIdToSeasons < ActiveRecord::Migration[5.2]
  def change
    add_reference :seasons, :tournament, foreign_key: true
  end
end
