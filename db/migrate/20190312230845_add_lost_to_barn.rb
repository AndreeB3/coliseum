class AddLostToBarn < ActiveRecord::Migration[5.2]
  def change
    add_column :barns, :lost, :integer
  end
end
