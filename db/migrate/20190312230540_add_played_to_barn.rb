class AddPlayedToBarn < ActiveRecord::Migration[5.2]
  def change
    add_column :barns, :played, :integer
  end
end
