class AddTieToBarn < ActiveRecord::Migration[5.2]
  def change
    add_column :barns, :tie, :integer
  end
end
