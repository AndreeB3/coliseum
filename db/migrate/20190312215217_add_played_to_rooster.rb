class AddPlayedToRooster < ActiveRecord::Migration[5.2]
  def change
    add_column :roosters, :played, :boolean
  end
end
