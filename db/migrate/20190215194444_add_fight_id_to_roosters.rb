class AddFightIdToRoosters < ActiveRecord::Migration[5.2]
  def change
    add_reference :roosters, :fight, foreign_key: true
  end
end
