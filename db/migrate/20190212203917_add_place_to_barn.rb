class AddPlaceToBarn < ActiveRecord::Migration[5.2]
  def change
    add_column :barns, :place, :string
  end
end
