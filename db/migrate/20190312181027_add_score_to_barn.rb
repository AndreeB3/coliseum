class AddScoreToBarn < ActiveRecord::Migration[5.2]
  def change
    add_column :barns, :score, :integer
  end
end
