class CreateFights < ActiveRecord::Migration[5.2]
  def change
    create_table :fights do |t|
      t.integer :number
      t.string :time
      t.integer :winner
      t.boolean :pollon
      t.references :season, foreign_key: true

      t.timestamps
    end
  end
end
