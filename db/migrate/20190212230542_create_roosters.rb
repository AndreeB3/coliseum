class CreateRoosters < ActiveRecord::Migration[5.2]
  def change
    create_table :roosters do |t|
      t.string :name
      t.string :weight
      t.string :plate
      t.string :type
      t.references :barn, foreign_key: true

      t.timestamps
    end
  end
end
