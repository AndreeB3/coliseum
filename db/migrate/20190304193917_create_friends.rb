class CreateFriends < ActiveRecord::Migration[5.2]
  def change
    create_table :friends do |t|
      t.integer :friend
      t.references :barn, foreign_key: true

      t.timestamps
    end
  end
end
