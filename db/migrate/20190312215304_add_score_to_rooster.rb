class AddScoreToRooster < ActiveRecord::Migration[5.2]
  def change
    add_column :roosters, :score, :integer
  end
end
